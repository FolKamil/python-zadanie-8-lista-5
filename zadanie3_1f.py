def main():
    n=int(input("Podaj n: "))
    if(n<=0):
        print("Podano zle dane!")
    else:
        licznik=0
        i=1
        while i<n+1:
            a=int(input("Podaj a: "))
            if a%2==0 and i%2==1:
                licznik+=1
            i+=1
        print("Liczb spelniajacych ten warunek jest: ", licznik)

main()
